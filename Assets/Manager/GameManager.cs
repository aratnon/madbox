﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using Random = UnityEngine.Random;


public class GameManager : MonoBehaviour
{
    public enum GameState
    {
        Play, GameOver, FinishLevel
    }

    [SerializeField] private CameraMovement camera;
    [SerializeField] private Player playerPrefab;
    [SerializeField] private List<Level> levelPrefabs;

    private Player _player;
    private Level _currentLevel;
    private int _countdownTime;

    public static GameManager Instance { get; private set; }
    public GameState CurrentGameState { get; private set; }
    
    void Awake() {
        if (Instance != null && Instance != this) {
            Destroy(gameObject);
            return;
        }
        
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }
    

    private void Start()
    {
        StartNewLevel();
    }

    private void StartNewLevel()
    {
        CreateLevel();
        CreatePlayer();
    }

    private Level GetRandomLevel()
    {
        return levelPrefabs[Random.Range(0, levelPrefabs.Count)];
    }
    
    private void CreateLevel()
    {
        Level level = GetRandomLevel();
        _currentLevel = Instantiate(level);
    }

    private void CreatePlayer()
    {
        if (_player != null)
        {
            Destroy(_player.gameObject);
        }
        
        _player = Instantiate(playerPrefab);
        var playerTransform = _player.transform;
        
        playerTransform.position = _currentLevel.startPosition;
        camera.SetTarget(playerTransform);
        CurrentGameState = GameState.Play;
    }

    public void GameOver()
    {
        Debug.Log("Game Over");
        camera.SetFollowTarget(false);
        CurrentGameState = GameState.GameOver;
        Invoke(nameof(CreatePlayer), 1.5f);
    }

    public void FinishLevel()
    {
        Debug.Log("Finish Level");
        CurrentGameState = GameState.FinishLevel;
        Invoke(nameof(NextLevel), 1.5f);
    }
    
    private void NextLevel()
    {
        Destroy(_currentLevel.gameObject);
        StartNewLevel();
    }
}
