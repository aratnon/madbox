﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [Range(0.01f, 1.0f)] [SerializeField] private float smoothFactor = 0.5f;
    
    [SerializeField] private Vector3 offset;
    
    private Transform _targetTransform;

    private bool _followTarget = true;

    public void SetFollowTarget(bool follow)
    {
        _followTarget = follow;
    }

    public void SetTarget(Transform target)
    {
        _targetTransform = target;
        SetFollowTarget(true);
    }
    
    void LateUpdate() {
        if (_followTarget)
        {
            var position = transform.position;
        
            Vector3 distancePos = _targetTransform.position + offset;
            transform.position = Vector3.Lerp(position, distancePos, smoothFactor);
        }
    }
}
