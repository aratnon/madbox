﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishLineCollision : MonoBehaviour
{
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (GameManager.Instance.CurrentGameState != GameManager.GameState.FinishLevel
                && GameManager.Instance.CurrentGameState != GameManager.GameState.GameOver)
            {
                GameManager.Instance.FinishLevel();
            }
        }
    }
}
