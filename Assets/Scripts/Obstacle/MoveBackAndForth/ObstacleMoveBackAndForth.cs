﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class ObstacleMoveBackAndForth : MonoBehaviour
{
    private enum TargetDirection
    {
        Position1, Position2
    }

    public enum MoveDirection
    {
        Left, Right
    }
    
    
    [SerializeField] private Vector3 position1, position2;
    [SerializeField] private float moveSpeed = 5f;
    private TargetDirection _targetDirection;
    private Vector3 _previousPosition;
    private Rigidbody _rigidbody;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        transform.position = position1;
        _targetDirection = TargetDirection.Position2;
    }

    private void FixedUpdate()
    {
        _previousPosition = transform.position;
        if (_targetDirection == TargetDirection.Position1)
        {
            MoveToward(position2);
            CheckReachPosition(position2);
        }
        else if (_targetDirection == TargetDirection.Position2)
        {
            MoveToward(position1);
            CheckReachPosition(position1);
        }
    }

    private void MoveToward(Vector3 position)
    {
        _rigidbody.MovePosition(Vector3.MoveTowards(transform.position, position, moveSpeed * Time.deltaTime));
    }

    private void CheckReachPosition(Vector3 targetPosition)
    {
        if (Vector3.Distance(transform.position, targetPosition) <= 0)
        {
            if (_targetDirection == TargetDirection.Position1)
            {
                _targetDirection = TargetDirection.Position2;
            }
            else
            {
                _targetDirection = TargetDirection.Position1;
            }
        }
    }

    public MoveDirection GetMoveDirection()
    {
        return transform.position.x < _previousPosition.x ? MoveDirection.Left : MoveDirection.Right;
    }
}
