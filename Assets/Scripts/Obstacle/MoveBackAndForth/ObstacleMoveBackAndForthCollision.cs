﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleMoveBackAndForthCollision : MonoBehaviour
{
    [Range(500, 2000)] [SerializeField] private float pushForce = 500f;
    
    private ObstacleMoveBackAndForth _obstacle;
    private void Awake()
    {
        _obstacle = GetComponent<ObstacleMoveBackAndForth>();
    }

    private void OnCollisionEnter(Collision other)
    {
        GameObject otherGameObject = other.gameObject;
        if (otherGameObject.CompareTag("Player"))
        {
            PlayerCollision playerCollision = otherGameObject.GetComponent<PlayerCollision>();
            if (!playerCollision.HitWithOther)
            {
                PushPlayer(_obstacle.GetMoveDirection(), otherGameObject);
            }
        }
    }

    private void PushPlayer(ObstacleMoveBackAndForth.MoveDirection direction, GameObject player)
    {
        var playerRigidbody = player.GetComponent<Rigidbody>();
        switch (direction)
        {
            case ObstacleMoveBackAndForth.MoveDirection.Left:
                playerRigidbody.AddForce(Vector3.left * pushForce);
                break;
            case ObstacleMoveBackAndForth.MoveDirection.Right:
                playerRigidbody.AddForce(Vector3.right * pushForce);
                break;
        }
    }
}
