﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private Rigidbody _rigidbody;

    private PlayerMovement _playerMovement;
    
    public PlayerMovement PlayerMovement => _playerMovement;
    
    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _playerMovement = GetComponent<PlayerMovement>();
    }
    
}
