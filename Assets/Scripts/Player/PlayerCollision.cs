﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    public bool HitWithOther { get; private set; }
    
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Obstacle"))
        {
            Debug.Log("Hit by Obstacle");
            
            HitWithOther = true;
            if (GameManager.Instance.CurrentGameState == GameManager.GameState.Play)
            {
                GameManager.Instance.GameOver();
            }
        }
    }
}
