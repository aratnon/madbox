﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
        [SerializeField] private float moveSpeed = 5f;

        private Rigidbody _rigidbody;
        private void Awake()
        {
                _rigidbody = GetComponent<Rigidbody>();
        }

        private void FixedUpdate() 
        {
                if (GameManager.Instance.CurrentGameState != GameManager.GameState.Play) return;
#if UNITY_EDITOR
                if (Input.GetMouseButton(0))
                {
                        MoveForward();
                }
#endif

#if UNITY_IOS || UNITY_ANDROID
                if (Input.touchCount > 0)
                {
                        MoveForward();
                }
#endif

        }
        
        private void MoveForward()
        {
                Vector3 movePos = transform.position + (moveSpeed * Time.deltaTime * Vector3.forward);
                _rigidbody.MovePosition(movePos);
        }
}
