The project doesn't have any user interface right now. You can see from the log when the game is over and finished.

### The time it took you to perform the exercise
  about 4 hours (I spent 30 mins downloaded Unity 2019.1.5f1)

### The parts that were difficult for you and why
 The physics part because I used to code only without the rigidbody (using only Translate function). I had to do a research a little bit about moving the rigidbody. 

### The parts you think you could do better and how
 Improve the physics interaction between player and obstacle by applying a right physics to the player. For example, the player should be push to the left when getting hit from the right.

### What you would do if you could go a step further on this game
 Define a theme for the game. For instance, the theme of game could be a small toy character running in each room in a house or in a garden, like Toy Story, or even ride a car or plane. The obstacles could be any items in a house. The game could sell character skins. 
Also, the game could have a survival game mode. Let's 100 players play at the same time. The player who dies will be eliminated from the game. There would be a leaderboard for this mode as well.

### Any comment you may have
The test was fun and reasonably challenging for 5 hours.# madbox

